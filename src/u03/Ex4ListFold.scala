package u03

import List._
import u03.Lists.List
import u03.Lists.List._

object Ex4ListFold {

  def foldLeft(list: List[Int])(v: Int)(f: (Int, Int)=> Int): Int = list match {
    case Cons(h, t) => foldLeft(t)(f(v,h))(f)
    case _ => v
  }


  def foldRight(list: List[Int])(v: Int)(f: (Int, Int)=> Int): Int = list match {
    case Cons(h, t) => f(h, foldRight(t)(v)(f))
    case _ => v
  }
}

object Ex4Main extends App {
  import Ex4ListFold._

  val lst = Cons(3, Cons(7, Cons(1, Cons(5, Nil()))))
  println(foldLeft(lst)(0)(_ - _))
  println(foldRight(lst)(0)(_ - _))
  println(foldLeft(lst)(2)(_ - _))
  println(foldRight(lst)(2)(_ - _))
}
