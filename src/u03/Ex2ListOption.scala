package u03
import List._
import u03.Lists.List
import u03.Lists.List._

import Option._
import u02.Optionals.Option
import u02.Optionals.Option._
import u02.OptionalsMain.{s1, s3}

object Ex2ListOption extends App{

  def max(l: List[Int]): Option[Int] = l match {

    case Cons(h, t) if (h >  getOrElse(max(t), 0) )=> Some(h)
    case Cons(h, t) if (h <=  getOrElse(max(t), 0) )=> Some(getOrElse(max(t), 0))
    case Nil() => None[Int]()
  }


  val s1: Option[Int] = Some(1)
  val s3: Option[Int] = None()

  println(s1) // Some(1)
  println(getOrElse(s1,0), getOrElse(s3,0)) // 1,0


  println(max( Cons (10 , Cons (25 , Cons (20 , Nil ()))))) // Some (25)
  println(max(Nil ())) // None ()

}
