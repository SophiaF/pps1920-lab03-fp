package u03

object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {

    case class Cons[E](head: E, tail: List[E]) extends List[E]

    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A, B](l: List[A])(mapper: A => B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = l1 match {
      case Cons(h, t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_, t) => filter(t)(pred)
      case Nil() => Nil()
    }

    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(h, t) if (n > 0) => drop(t, n - 1)
      case _ => l
    }

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] = l match {
      case Cons(h, t) => append(f(h), flatMap(t)(f))
      case _ => Nil()
    }

    def map2[A, B](l: List[A])(mapper: A => B): List[B] = {
      flatMap(l)(s => Cons(mapper(s), Nil()))
    }

    def filter2[A](l: List[A])(pred: A => Boolean): List[A] =
      flatMap(l)(h => pred(h) match {
        case y if y => Cons(h, Nil())
        case _ => Nil()
      })
  }
}

object ListsMain extends App {

  import Lists._
  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60
  import List._
  import u03.Lists.List
  println(append(Cons(5, Nil()), l)) // 5,10,20,30
  println(filter[Int](l)(_ >=20)) // 20,30
  println(drop (l ,1)) // Cons (20 , Cons (30 , Nil ()))
  println(drop (l ,2)) // Cons (30 , Nil ())
  println(drop (l ,5))
  println(flatMap (l)(v => Cons (v+1, Nil ())))
  println(flatMap (l)(v => Cons (v+1, Cons (v+2, Nil ()))))
  println(map(filter(l)(_>=20))(_+1))
  println("Prove Map2: " + map2(l)(_+1))

  println("Prove Filter2: " + filter2(l)(_>=20))
  println("Map2 + Filter2: " + map2(filter2(l)(_>=20))(_+1))

}