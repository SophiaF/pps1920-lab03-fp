package u03

import List._
import u03.Lists.List
import u03.Lists.List._

object Ex3Person extends App {

  sealed trait Person // sealed : no other impl . except Student , Teacher
  case class Student ( name : String , year : Int ) extends Person
  case class Teacher ( name : String , course : String ) extends Person

  def name (p: Person ): String = p match {
    case Student (n,_) => n
    case Teacher (n,_) => n
  }

  //filter[A](l1: List[A])(pred: A => Boolean): List[A]
  //map[A, B](l: List[A])(mapper: A => B): List[B]

  def getCourse(lp: List[Person]): List[String] = {
    map(filter[Person](lp)( p => p.isInstanceOf[Teacher]))(_ match {
      case Teacher(n,c) => c
      case _ => ""
    })
  }
  //def flatMap[A, B](l: List[A])(f: A => List[B]): List[B]
  def getCourse2(lp: List[Person]): List[String] = {
    flatMap(lp)(p => p match {
      case Teacher(n,c) =>Cons(c, Nil())
      case _ => Nil()
    })
  }

  /*def getCourse(lp: List[Person]): List[String] = lp match {
    case Cons(h, t) => h match {
      case Teacher(n, c) => Cons(c, getCourse(t))
      case Student(n, c) => getCourse(t)
    }
    case Nil() => Nil()
  }*/

  val doc1:Person = Teacher("Doc1", "Programmazione")
  val doc2:Person = Teacher("Doc2", "Paradigmi")
  val stu1:Person = Student("Stu1", 1996)
  val persons: List[Person] = Cons(doc1, Cons(doc2, Cons(stu1, Nil())))
  println(getCourse(persons))
  println(getCourse2(persons))

 // println ( name ( Student (" mario " ,2015) ))
  //val l = List.Cons( Teacher (" mario " ,2015), List.Cons( Student (" mario " ,2015), List.Cons( Student (" mario " ,2015), List.Nil())))
 // val l = List.Cons(Student("Mario", 2000), List.Cons(Teacher("Rossi", "Iot"), List.Cons(Teacher("Bianchi", "web"), List.Nil())))

  //println(getCourse(l))

}
